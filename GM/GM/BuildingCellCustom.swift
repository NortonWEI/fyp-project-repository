//
//  BuildingCellCustom.swift
//  Location Tracker
//
//  Created by WEI Wenzhou on 17/10/2016.
//  Copyright © 2016 Wenzhou Wei. All rights reserved.
//

import Foundation
import UIKit

class BuildingCellCustom:UITableViewCell {
    
    @IBOutlet weak var buildingImageView: UIImageView!
    @IBOutlet weak var buildingNameLabel: UILabel!
    @IBOutlet weak var buildingCampusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
