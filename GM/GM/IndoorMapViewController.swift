//
//  IndoorMapViewController.swift
//  GM
//
//  Created by WEI Wenzhou on 14/10/2016.
//  Copyright © 2016 Wenzhou Wei. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import MapKit

class IndoorMapViewController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var navigationTitle: UINavigationItem!
    
    let locationManager = CLLocationManager()
    let region = CLBeaconRegion(proximityUUID: NSUUID(uuidString: "E2C56DB5-DFFB-48D2-B060-D0F5A71096E0") as! UUID, major: 10002, minor: 5208, identifier: "default")
    
    var building: Building?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.startRangingBeacons(in: region)
        navigationTitle.title = building?.name
        print(building?.mkMeter)
        print(building?.pixelDistance)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        print(beacons)
        print(beacons.first?.accuracy)
    }
}
