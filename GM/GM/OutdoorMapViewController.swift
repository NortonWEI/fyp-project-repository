//
//  OutdoorMapViewController.swift
//  GM
//
//  Created by Wenzhou Wei on 26/8/2016.
//  Copyright © 2016 Wenzhou Wei. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import CoreBluetooth

class OutdoorMapViewController: UIViewController,CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    @IBOutlet weak var mapView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.showCurrentLocation()
        self.locationManager.stopUpdatingLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showCurrentLocation () {
        let camera = GMSCameraPosition.camera(withLatitude: self.locationManager.location!.coordinate.latitude, longitude: self.locationManager.location!.coordinate.longitude, zoom: 14)
        let mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.mapView.frame.size.width, height: self.mapView.frame.size.height), camera: camera)
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        self.mapView.addSubview(mapView)
    }
}
