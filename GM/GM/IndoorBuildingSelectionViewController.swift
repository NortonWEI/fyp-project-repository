//
//  IndoorBuildingSelectionViewController.swift
//  Location Tracker
//
//  Created by WEI Wenzhou on 15/10/2016.
//  Copyright © 2016 Wenzhou Wei. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class IndoorBuildingSelectionViewController: UIViewController {
    
    @IBOutlet weak var buildingTableView: UITableView!
    
    private var fsc = Building(id: 0, name: "Fong Shu Chuen Library", campus: "Ho Sin Hang Campus", buildingImageName: "fsc", northEast: MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 22.340338, longitude: 114.180272)), southEast: MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 22.340105, longitude: 114.180075)))
    private var rrs = Building(id: 1, name: "Sir Run Run Shaw Building", campus: "Ho Sin Hang Campus", buildingImageName: "rrs", northEast: MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 22.340338, longitude: 114.180272)), southEast: MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 22.340105, longitude: 114.180075)))
    private var aab = Building(id: 2, name: "Academic and Administration Building", campus: "Baptist Univeristy Road Campus", buildingImageName: "aab", northEast: MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 22.340338, longitude: 114.180272)), southEast: MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 22.340105, longitude: 114.180075)))
    private var cva = Building(id: 3, name: "Communication and Visual Arts Building", campus: "Baptist Univeristy Road Campus", buildingImageName: "cva", northEast: MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 22.340338, longitude: 114.180272)), southEast: MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 22.340105, longitude: 114.180075)))
    private var sct = Building(id: 4, name: "Cha Chi-ming Science Tower", campus: "Ho Sin Hang Campus", buildingImageName: "sct", northEast: MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 22.340338, longitude: 114.180272)), southEast: MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 22.340105, longitude: 114.180075)))
    private var oen = Building(id: 5, name: "Oen Hall Building", campus: "Ho Sin Hang Campus", buildingImageName: "oen", northEast: MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 22.340338, longitude: 114.180272)), southEast: MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 22.340105, longitude: 114.180075)))
    private var lmc = Building(id: 6, name: "Lui Ming Choi Centre", campus: "Ho Sin Hang Campus", buildingImageName: "lmc", northEast: MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 22.340338, longitude: 114.180272)), southEast: MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 22.340105, longitude: 114.180075)))
    private var whsc = Building(id: 7, name: "Wai Hang Sports Centre", campus: "Ho Sin Hang Campus", buildingImageName: "whsc", northEast: MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 22.340338, longitude: 114.180272)), southEast: MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 22.340105, longitude: 114.180075)))
    private var wlb = Building(id: 8, name: "The Wing Lung Bank Building", campus: "Shaw Campus", buildingImageName: "wlb", northEast: MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 22.340338, longitude: 114.180272)), southEast: MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 22.340105, longitude: 114.180075)))
    private var dlb = Building(id: 9, name: "David C. Lam Building", campus: "Shaw Campus", buildingImageName: "dlb", northEast: MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 22.340338, longitude: 114.180272)), southEast: MKMapPointForCoordinate(CLLocationCoordinate2D(latitude: 22.340105, longitude: 114.180075)))
    
    var buildings: [Building]!
    var chosenBuildingIndex = 0
    
    let searchController = UISearchController(searchResultsController: nil)
    var filteredBuildings: [Building]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.sizeToFit()
        buildingTableView.tableHeaderView = searchController.searchBar
        buildings = [fsc, rrs, sct, oen, lmc, whsc, wlb, dlb, aab, cva]
        self.buildingTableView.dataSource = self
        self.buildingTableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.buildingTableView.dataSource = self
        buildingTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showIndoorMapSegue" {
            if let indexPathForSelectedRow = self.buildingTableView.indexPathForSelectedRow {
                chosenBuildingIndex = (indexPathForSelectedRow as NSIndexPath).row
            }
        }
        
        let destinationViewController = segue.destination as? IndoorMapViewController
        let building: Building
        if searchController.isActive && searchController.searchBar.text != "" {
            building = filteredBuildings![chosenBuildingIndex]
        } else {
            building = buildings[chosenBuildingIndex]
        }
        
        if let destinationViewController = destinationViewController {
            destinationViewController.building = building
        }
    }
    
    func filterContent(searchText: String) {
        filteredBuildings = buildings.filter{ building in
            return building.name.lowercased().contains(searchText.lowercased())
        }
        buildingTableView.reloadData()
    }
}

extension IndoorBuildingSelectionViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredBuildings!.count
        } else {
            return buildings.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "buildingCell"
        let cell = buildingTableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! BuildingCellCustom
        let building: Building
        if searchController.isActive && searchController.searchBar.text != "" {
            building = filteredBuildings![(indexPath as NSIndexPath).row]
        } else {
            building = buildings[(indexPath as NSIndexPath).row]
        }
        cell.buildingNameLabel.text = building.name
        cell.buildingCampusLabel.text = building.campus
        cell.buildingImageView.image = UIImage(named: building.buildingImageName)
        
        return cell
    }
}

extension IndoorBuildingSelectionViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContent(searchText: searchController.searchBar.text!)
    }
}
