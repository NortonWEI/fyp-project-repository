//
//  User.swift
//  GM
//
//  Created by WEI Wenzhou on 14/10/2016.
//  Copyright © 2016 Wenzhou Wei. All rights reserved.
//

import Foundation
import CoreLocation

class User {
    
    let id: Int
    let name: String
    let location: CLLocation
    
    init(id: Int, name: String, location: CLLocation) {
        self.id = id
        self.name = name
        self.location = location
    }
}
