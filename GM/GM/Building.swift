//
//  Building.swift
//  Location Tracker
//
//  Created by WEI Wenzhou on 15/10/2016.
//  Copyright © 2016 Wenzhou Wei. All rights reserved.
//

import Foundation
import MapKit

class Building {
    
    let id: Int
    let name: String
    let campus: String
    let buildingImageName: String
    let northEast: MKMapPoint
    let southEast: MKMapPoint
    let mkMeter: CLLocationDistance
    let pixelDistance: Double
    let meterScale: CLLocationDistance
    
    init(id: Int, name: String, campus: String, buildingImageName: String, northEast: MKMapPoint, southEast: MKMapPoint ) {
        self.id = id
        self.name = name
        self.campus = campus
        self.buildingImageName = buildingImageName
        self.northEast = northEast
        self.southEast = southEast
        self.mkMeter = MKMetersBetweenMapPoints(northEast, southEast)
        self.pixelDistance = hypot(1000, 1000)
        self.meterScale = MKMetersPerMapPointAtLatitude(northEast.x)
    }
    
//    func getMkMeter() -> CLLocationDistance {
//        return mkMeter
//    }
//    
//    func getBuildingName() -> String {
//        return name
//    }
}
